//
//  Order.m
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "Order.h"

@implementation Order

- (instancetype)init
{
    if ((self = [super init])) {
        self.dateOfOrder = [NSDate date];
        self.confirm = NO;
        self.canceled = NO;
        self.order = [NSMutableArray arrayWithCapacity:1];
    }
    
    return self;
}

@end
