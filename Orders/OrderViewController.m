//
//  FirstViewController.m
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "OrderViewController.h"
#import "OrdersStore.h"
#import "OrderList.h"
#import "Order.h"
#import "Dish.h"
#import "OrderTableViewCell.h"
#import "UIImage+Resize.h"
#import "HeaderCell.h"
#import "DetailOrderViewController.h"

@interface OrderViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) OrdersStore *orderStoreNow;
@property (nonatomic, strong) OrdersStore *orderStoreUpcomeing;

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.separatorColor = [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1];
    self.orderStoreNow = [OrdersStore new];
    self.orderStoreUpcomeing = [OrdersStore new];
    
    Order *order1 = [Order new];
    order1.orderer = @"Dane Parker";
    order1.thumbnailName = @"thumbnail1";
    order1.hasThumbnail = YES;
    order1.numberOfTable = 3;
    
    Dish *dish1 = [Dish new];
    dish1.dishName = @"Classic Burger";
    dish1.dishDetail = @"With fresh fries";
    dish1.price = @9.99;
    [order1.order addObject:dish1];
    
    Dish *dish2 = [Dish new];
    dish2.dishName = @"Pasrta";
    dish2.dishDetail = @"Carbonara";
    dish2.additionalInformation = @"Notify if item is unavailable";
    dish2.price = @15.00;
    [order1.order addObject:dish2];
    
    
    if ([[NSDate date] timeIntervalSinceDate:order1.dateOfOrder] < 60) {
        [self.orderStoreNow.orders addObject:order1];
    } else {
        [self.orderStoreUpcomeing.orders addObject:order1];
    }
    
    Order *order2 = [Order new];
    order2.orderer = @"Dane Parkinson";
    order2.thumbnailName = @"thumbnail1";
    order2.hasThumbnail = YES;
    order2.numberOfTable = 5;
    
    Dish *dish3 = [Dish new];
    dish3.dishName = @"Burger";
    dish3.dishDetail = @"With cola";
    dish3.price = @8.99;
    [order2.order addObject:dish3];
    
    Dish *dish4 = [Dish new];
    dish4.dishName = @"Chips";
    dish4.dishDetail = @"with fish";
    dish4.additionalInformation = @"Notify if item is unavailable";
    dish4.price = @10.00;
    [order2.order addObject:dish4];
    
    if ([[NSDate date] timeIntervalSinceDate:order2.dateOfOrder] < 60) {
        [self.orderStoreNow.orders addObject:order2];
    } else {
        [self.orderStoreUpcomeing.orders addObject:order2];
    }
    
    for (Order *order in self.orderStoreNow.orders) {
        NSLog(@"%@", order.orderer);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditSegue"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        
        DetailOrderViewController *detailController = (DetailOrderViewController *)navigationController.topViewController;
        
        NSIndexPath *index = [self.tableView indexPathForCell:sender];
        
        if (index.section == 0) {
            detailController.order = self.orderStoreNow.orders[index.row];
        } else {
            detailController.order = self.orderStoreUpcomeing.orders[index.row];
        }
    }
}

#pragma - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [self.orderStoreNow.orders count];
    } else {
        return [self.orderStoreUpcomeing.orders count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)timeLeftSinceDate:(NSDate *)dateT
{
    NSString *timeLeft;
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dateT];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) {
        return nil;
    }
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) {
        return nil;
    }
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(minutes) {
        timeLeft = [NSString stringWithFormat: @"%ld", (long)minutes*-1];
        return timeLeft;
    } else if(seconds) {
        return @"Now";
    }
    
    return nil;
}

- (OrderTableViewCell *)configureCell:(OrderTableViewCell *)cell withOrder:(Order *)order andSection:(NSInteger)section
{
    cell.nameLabel.text = order.orderer;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.timeStyle = NSDateFormatterShortStyle;
    NSString *time = [formatter stringFromDate:order.dateOfOrder];
    
    cell.detailsOfOrderLabel.text = [NSString stringWithFormat:@"%@, table for %d", time, order.numberOfTable];
    
    NSString *dateDiff = [self timeLeftSinceDate:order.dateOfOrder];
    
    if (section != 0) {
        if (dateDiff != nil || ![dateDiff isEqualToString:@"Now"]) {
            cell.nowLabel.text = [NSString stringWithFormat:@"%@ min ago", dateDiff];
        } else {
            cell.nowLabel.text = time;
        }
    } else if (section != 1) {
        cell.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:194.0/255.0 blue:130.0/255.0 alpha:1];
        cell.nameLabel.textColor = [UIColor whiteColor];
        
        if ([dateDiff isEqualToString:@"Now"]) {
            cell.nowLabel.text = dateDiff;
        }
    }
    
    if (order.hasThumbnail) {
        UIImage *image = [[UIImage imageNamed:@"thumbnail1"] resizeImageWithBounds:CGSizeMake(80, 80)];
        cell.thumbnailView.image = image;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderTableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"OrderTableViewCell"];
    
    if (indexPath.section == 0) {
        Order *order = self.orderStoreNow.orders[indexPath.row];
        return [self configureCell:cell withOrder:order andSection:0];
    } else {
        Order *order = self.orderStoreUpcomeing.orders[indexPath.row];
        return [self configureCell:cell withOrder:order andSection:1];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"Header"];
    
    if (section == 0) {
        headerCell.nameLabel.text = @"NEW";
    } else {
        headerCell.nameLabel.text =@"UPCOMING";
    }
    
    return headerCell;
}

@end
