//
//  Dish.h
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dish : NSObject

@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *dishDetail;
@property (nonatomic, strong) NSString *additionalInformation;
@property (nonatomic, strong) NSDecimalNumber *price;

@end
