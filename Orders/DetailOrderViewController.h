//
//  DetailOrderViewController.h
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Order;

@interface DetailOrderViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) Order *order;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *CancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *ConfirmButton;
- (IBAction)closeButtonPressed:(id)sender;

@end
