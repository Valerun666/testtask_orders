//
//  OrdersList.m
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "OrdersStore.h"

@implementation OrdersStore

- (instancetype)init
{
    if ((self = [super init])) {
        self.orders = [NSMutableArray arrayWithCapacity:10];
    }
    
    return self;
}

@end
