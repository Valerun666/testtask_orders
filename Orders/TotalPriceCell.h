//
//  TotalPriceCell.h
//  Orders
//
//  Created by Valerun on 12.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalPriceCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *totalPrice;

@end
