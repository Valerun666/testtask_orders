//
//  OrderList.h
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderList : NSObject

@property (nonatomic, strong) NSMutableArray *listOfOrders;

@end
