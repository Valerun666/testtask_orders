//
//  PriseDetailCell.h
//  Orders
//
//  Created by Valerun on 12.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriseDetailCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *subtotalLabel;
@property (nonatomic, weak) IBOutlet UILabel *taxLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipsLabel;

@end
