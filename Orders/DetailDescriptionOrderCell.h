//
//  DetailDescriptionOrderCell.h
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailDescriptionOrderCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *orderDetailLabel;
@property (nonatomic, weak) IBOutlet UILabel *additionalInformationLabel;

@end
