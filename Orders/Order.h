//
//  Order.h
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OrderList;

@interface Order : NSObject

@property (nonatomic, strong) NSString *orderer;
@property (nonatomic, strong) NSDate *dateOfOrder;
@property (nonatomic, strong) NSMutableArray *order;
@property (nonatomic, strong) NSString *thumbnailName;
@property (nonatomic, assign) BOOL hasThumbnail;
@property (nonatomic, assign) int numberOfTable;
@property (nonatomic, assign) BOOL *confirm;
@property (nonatomic, assign) BOOL *canceled;

@end
