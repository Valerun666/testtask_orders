//
//  DetailOrderViewController.m
//  Orders
//
//  Created by Valerun on 11.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "DetailOrderViewController.h"
#import "Order.h"
#import "DetailOrderHeaderCell.h"
#import "DetailDescriptionOrderCell.h"
#import "Dish.h"
#import "DishDetailCell.h"
#import "DishDetailWithInfoCell.h"
#import "PriseDetailCell.h"
#import "TotalPriceCell.h"
#import "RefundCell.h"

@implementation DetailOrderViewController {
    int _rowCount;
    NSString *_statusText;
    UIColor *_statusColor;
    UIColor *_aditionalInfoLabelColor;
    UIImageView *_navBarHairlineImageView;
    CGFloat _firstRowHeight;
    double _totalPrice;
    UIView *_footerView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]];
    
    self.tableView.separatorColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];
    self.title = [self.order.orderer uppercaseString];
    
    [self.CancelButton setBackButtonBackgroundImage:[UIImage imageNamed:@"CancelButton"] forState:UIControlStateNormal barMetrics:0];
    
    _rowCount = [self.order.order count] + 3;
    
    if (self.order.canceled == YES) {
        _statusText = @"ORDER CANCELED BY RESTAURANT";
        _statusColor = [UIColor colorWithRed:255.0/255.0 green:171.0/255.0 blue:64.0/255.0 alpha:1];
        _firstRowHeight = 44.0;
    } else {
        if (self.order.confirm) {
            _statusText = @"ORDER IS BEING PREPARED";
            _statusColor = [UIColor colorWithRed:19.0/255.0 green:184.0/255.0 blue:110.0/255.0 alpha:1];
            _firstRowHeight = 44.0;
        } else {
            _statusText = @"WAITING FOR YOUR CONFIRMATION";
            _statusColor = [UIColor colorWithRed:255.0/255.0 green:171.0/255.0 blue:64.0/255.0 alpha:1];
            _firstRowHeight = 60.0;
        }
    }

    
    _navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _navBarHairlineImageView.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _navBarHairlineImageView.hidden = NO;
}

- (void)refundButtonPressed
{
    NSLog(@"refundButtonPressed");
}

- (void)confirmButtonPressed
{
    self.order.confirm = YES;
    
    _statusText = @"ORDER IS BEING PREPARED";
    _statusColor = [UIColor colorWithRed:19.0/255.0 green:184.0/255.0 blue:110.0/255.0 alpha:1];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma - UITableViewControllerDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        if (tableView == self.tableView) {    // self.tableview
            CGFloat cornerRadius = 5.f;
            cell.backgroundColor = UIColor.clearColor;
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CGMutablePathRef pathRef = CGPathCreateMutable();
            CGRect bounds = CGRectInset(cell.bounds, 5, 0);
            BOOL addLine = NO;
            if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            } else if (indexPath.row == 0) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            layer.path = pathRef;
            CFRelease(pathRef);
            layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+5, bounds.size.height-lineHeight, bounds.size.width-5, lineHeight);
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            UIView *testView = [[UIView alloc] initWithFrame:bounds];
            [testView.layer insertSublayer:layer atIndex:0];
            testView.backgroundColor = UIColor.clearColor;
            cell.backgroundView = testView;
        }
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    
    if (indexPath.row == 0) {
        DetailDescriptionOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailDescriptionOrderCell" forIndexPath:indexPath];
        
        if (!cell) {
            cell = [DetailDescriptionOrderCell new];
        }
        
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.timeStyle = NSDateFormatterShortStyle;
        NSString *time = [formatter stringFromDate:self.order.dateOfOrder];
        
        cell.orderDetailLabel.text = [NSString stringWithFormat:@"%@, table for %d", time, self.order.numberOfTable];
        
        if (!self.order.confirm) {
            cell.additionalInformationLabel.text = @"MOVE TO ANOTHER TIME";
        } else {
            cell.additionalInformationLabel.text = @"";
        }
        
        return cell;
    } else if (indexPath.row >= 1 && indexPath.row <= [self.order.order count]) {
        if (!self.order.confirm) {
            Dish *dish = self.order.order[indexPath.row - 1];
            
            if (!dish.additionalInformation) {
                DishDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishDetailCell" forIndexPath:indexPath];
                
                if (!cell) {
                    cell = [DishDetailCell new];
                }
                
                cell.nameLabel.text = dish.dishName;
                cell.detailLabel.text = dish.dishDetail;
                cell.priceLabel.text = [NSString stringWithFormat:@"$%@",dish.price];
                
                return cell;
            } else {
                DishDetailWithInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishDetailWithInfoCell" forIndexPath:indexPath];
                
                if (!cell) {
                    cell = [DishDetailWithInfoCell new];
                }
                
                cell.nameLabel.text = dish.dishName;
                cell.detailLabel.text = dish.dishDetail;
                cell.priceLabel.text = [NSString stringWithFormat:@"$%@",dish.price];
                cell.aditionalInfoLabel.text = dish.additionalInformation;
                
                return cell;
            }
        } else {
            Dish *dish = self.order.order[indexPath.row - 1];
            
            DishDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DishDetailCell" forIndexPath:indexPath];
            
            if (!cell) {
                cell = [DishDetailCell new];
            }
            
            cell.nameLabel.text = dish.dishName;
            cell.detailLabel.text = dish.dishDetail;
            cell.priceLabel.text = [NSString stringWithFormat:@"$%@",dish.price];
            
            return cell;
        }
    } else if (indexPath.row == [self.order.order count] + 1){
        double result;
        
        for (Dish *dish in self.order.order) {
            result += [dish.price floatValue];
        }
        
        double tex = result * 0.2;
        double tips = (result - tex) * 0.2;
        
        PriseDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PriseDetailCell" forIndexPath:indexPath];
        
        if (!cell) {
            cell = [PriseDetailCell new];
        }
        
        double subtotal = result - tex;
        
        _totalPrice = result + tips;
        
        cell.subtotalLabel.text = [NSString stringWithFormat:@"$%.2f", subtotal];
        cell.taxLabel.text = [NSString stringWithFormat:@"$%.2f", tex];
        cell.tipsLabel.text = [NSString stringWithFormat:@"$%.2f", tips];
        
        return cell;
    } else {
        TotalPriceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TotalPriceCell"];
        
        if (!cell) {
            cell = [TotalPriceCell new];
        }
        
        cell.totalPrice.text = [NSString stringWithFormat:@"$%.2f",_totalPrice];
        
        return cell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60.0;
    } else if (indexPath.row >= 1 && indexPath.row <= [self.order.order count]){
        return 80.0;
    } else if (indexPath.row == [self.order.order count] + 1) {
        return 90.0;
    } else {
        return 44.0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DetailOrderHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"DetailOrderHeaderCell"];
    
    
    headerCell.statusLabel.text = _statusText;
    headerCell.statusLabel.textColor = _statusColor;
    
    return headerCell.contentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    _footerView = nil;
    if (self.order.canceled) {
        return _footerView;
    }
    
    if (self.order.confirm) {
        _footerView = [UIView new];
        
        UIButton *refundButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        refundButton.backgroundColor = self.tableView.separatorColor;
        refundButton.layer.borderWidth = 1;
        refundButton.layer.borderColor = [self.tableView.tintColor CGColor];
        [refundButton setTitle:@"REFUND" forState:UIControlStateNormal];
        
        refundButton.layer.cornerRadius = 5;
        
        refundButton.frame = CGRectMake(3, 5, self.tableView.bounds.size.width - 6, 40);
        [refundButton addTarget:self action:@selector(refundButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_footerView addSubview:refundButton];
    } else {
        _footerView = [UIView new];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        cancelButton.backgroundColor = [UIColor colorWithRed:197.0/255.0 green:205.0/255.0 blue:208.0/255.0 alpha:1];
        [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
        cancelButton.titleLabel.textColor = [UIColor whiteColor];
        
        cancelButton.frame = CGRectMake(0,
                                        5,
                                        self.tableView.bounds.size.width / 2,
                                        40);
        
        [cancelButton addTarget:self action:@selector(cancelButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_footerView addSubview:cancelButton];
        
        UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        confirmButton.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:125.0/255.0 blue:99.0/255.0 alpha:1];
        [confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
        confirmButton.titleLabel.textColor = [UIColor whiteColor];
        confirmButton.frame = CGRectMake(CGRectGetMaxX(cancelButton.frame),
                                        5,
                                        self.tableView.bounds.size.width / 2,
                                        40);
        
        [confirmButton addTarget:self action:@selector(confirmButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_footerView addSubview:confirmButton];
    }
    
    return _footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}


- (IBAction)closeButtonPressed:(id)sender {
    [self closeScreen];
}

- (void)closeScreen
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelButtonPressed
{
    self.order.canceled = YES;
    _statusText = @"ORDER CANCELED BY RESTAURANT";
    
//    [self.tableView headerViewForSection:0].statusLabel.text = @"ORDER CANCELED BY RESTAURANT";
    
    [self.tableView beginUpdates];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
